package semana05;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class EjemploEmisor {
	
	// Una aplicación que manda un mensaje utilizando un socket datagrama
	// sin conexión.
	// Se esperan tres argumentos de línea de mandato, en orden:
	// <nombre de dominio o dirección IP del recepetor>
	// <número del puerto del socket del receptor>
	// <mensaje, una cadena, para mandar>
	
	public static void main(String[ ] args) {
		if (args. length != 3)
			System.out.println("Este programa requiere 3 argumentos de linea de mandato");
		else {
			try {
				InetAddress maquinaReceptora = InetAddress. getByName (args [ 0 ]) ;
				int puertoReceptor = Integer.parseInt(args[1]);
				String mensaje = args[2];
				
				// instancia un socket datagrama para mandar los datos
				DatagramSocket miSocket = new DatagramSocket ( );
				byte[ ] almacen = mensaje.getBytes( );
				DatagramPacket datagrama = new DatagramPacket(almacen, almacen.length,maquinaReceptora, puertoReceptor);
				miSocket.send(datagrama);
				miSocket.close( );
			}
			catch (Exception ex) {
				ex.printStackTrace( ) ;
			}

		}
	}

}
