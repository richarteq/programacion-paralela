package semana05;

public class EjecHilos {
	
	public static void main(String[] args) {
		
		HiloEjemplo p1 = new HiloEjemplo(1);
		p1.start();
		
		HiloEjemplo p2 = new HiloEjemplo(2);
		p2.start();
		
		HiloEjemplo p3 = new HiloEjemplo(3);
		p3.start();
		
	}

}
