package semana05;

import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class EjemploReceptor {

	public static void main(String[] args){
		
		// Una aplicación que recibe un mensaje utilizando un socket datagrama
		// sin conexión.
		// Se espera un argumento de línea de mandato:
		// <número de puerto del socket del recptor>
		// Note: se debería especificar el mismo número de puerto
		// en los argumentos de línea de mandato del emisor.
		
		if (args. length != 1)
		System.out.println("Este programa requiere 1 argumentos de linea de mandato");
		else {
			int puerto = Integer.parseInt(args[ 0 ]);
			final int MAX_LON = 10 ;
			// Esta es la longitud máxima asumida en octetos
			// del datagrama que se va recibir.
			try {
				DatagramSocket miSocket = new DatagramSocket(puerto);
				// Instancia un socket datagrama para recibir los datos
				byte[] almacen = new byte[MAX_LON];
				DatagramPacket datagrama = new DatagramPacket (almacen, MAX_LON );
				miSocket.receive(datagrama);
				String mensaje = new String(almacen);
				System. out.println(mensaje);
				miSocket.close( );
			}
			catch (Exception ex) {
				ex.printStackTrace( ) ;
			}

		}
	
	}
	
}
