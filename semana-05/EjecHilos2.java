package semana05;

public class EjecHilos2 {
	
	public static void main(String[] args) {
		
		Thread p1 = new Thread(new HiloEjemplo2(1));
		p1.start();
		
		Thread p2 = new Thread(new HiloEjemplo2(2));
		p2.start();
		
		Thread p3 = new Thread(new HiloEjemplo2(3));
		p3.start();
		
	}

}
