package semana04;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

import com.panayotis.gnuplot.JavaPlot;

public class TestMultMatrSec4 {
	
	
	@SuppressWarnings("null")
	public static void main(String[] args) throws FileNotFoundException {
		
		//int N=2;
		
		int N;

        @SuppressWarnings("resource")
		Scanner teclado = new Scanner( System.in );
        System.out.print( "Introduzca el tamaño máximo para generar matrices cuadradas: " );
        N = teclado.nextInt();
        
        String archivoInsercion = "multMatrSec.txt";
        PrintWriter oS = new PrintWriter(archivoInsercion);
        
        //ArrayList<Long> Ts = new ArrayList<Long>();
        
        int[][] a = null;
		int[][] b = null;
		int valor;
		int i;
		int[][] c = null;
		
		for(int s=2; s<=N;s++) {
			a = new int[s][s];
			b = new int[s][s];
			
			valor = 1;
			for ( i = 0; i < s; i++) {
	            for (int j = 0; j < s; j++) {
	            	a[i][j] = valor++;
	            }
	        }
			for ( i = 0; i < s; i++) {
	            for (int j = 0; j < s; j++) {
	            	b[i][j] = valor++;
	            }
	        }
			long start = System.nanoTime();
			c = new MultMatrSec2(a,b).multiply();
			long end = System.nanoTime() - start;
			//System.out.println("Tiempo total de ejecucion T("+s+","+s+") : " + end + " ns");
			oS.println(end);
		}
		oS.close();
		
		JavaPlot p = new JavaPlot();
		p.addPlot("\"/home/richart/eclipse-workspace/CDP_21b/multMatrSec.txt\" with lines");
		p.plot();
		
		
		//int[][] c = new int[N][N];
						
		//imprimirMatr(a);
		//imprimirMatr(b);
				
		//imprimirMatr(c);

	}
	
	public static void imprimirMatr(int[][] a) {
		System.out.println("");
		for (int j = 0; j < a[0].length; j++) {
			System.out.println("");
            for (int k = 0; k < a[0].length; k++) {                
                System.out.print(a[j][k] + " ");
            }
		}
	}
	
	

}
