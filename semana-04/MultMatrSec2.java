package semana04;

public class MultMatrSec2 {
	
	int[][] a;
	int[][] b;
	
	public MultMatrSec2(int[][] A, int[][] B) {
		this.a = A;
		this.b = B;
		
	}
	
	public int[][] multiply() {
	    int[][] c = new int[this.a.length][b[0].length];
	    // se comprueba si las matrices se pueden multiplicar
	    if (a[0].length == this.b.length) {
	    	
	    	
	    	long start2, end2;
	    	long start = System.nanoTime();
	        for (int i = 0; i < this.a.length; i++) {
	            for (int j = 0; j < this.b[0].length; j++) {
	            	//System.out.println("");
	            	start2 = System.nanoTime();
	                for (int k = 0; k < this.a[0].length; k++) {
	                    // aquí se multiplica la matriz
	                    c[i][j] += this.a[i][k] * this.b[k][j];
	                }
	                end2 = System.nanoTime() - start2;
	                //System.out.println("t("+i+","+j+") :" + end2 + " ns");
	            }
	        }
	        long end = System.nanoTime() - start;
	        //System.out.println("Tiempo total de ejecucion T: " + end + " ns");
	    }
	    /**
	     * si no se cumple la condición se retorna una matriz vacía
	     */
	    return c;
	}
	
	

}
