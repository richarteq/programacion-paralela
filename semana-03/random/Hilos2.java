package semana03;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Hilos2 {

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		
		Scanner teclado = new Scanner(System.in);
	    
	    System.out.println("Ingrese cantidad de caballos : ");
	    int n1 = Integer.parseInt(teclado.nextLine()); 
		
	    ArrayList<String> nombres = new ArrayList<String>(
	    	    Arrays.asList("Tornado", "Pegaso", "Socrates","Espiritu","Galan"));
	    
	    ArrayList<Integer> veces = new ArrayList<Integer>();
	    for(int v=1; v<=nombres.size(); v++) {
	    	veces.add(0);
	    }
	    
	    int randomNum;
	    
	    for(int i=1; i<=n1; i++) {
	    	/*
	    	 * asignar nombres aleatorios a cada caballo*/
	    	randomNum = 0 + (int)(Math.random() * (nombres.size()-1));
	    	veces.set(randomNum, veces.get(randomNum)+1);
	    	new OtroHilo2(nombres.get(randomNum) + "_" + veces.get(randomNum)).start();
	    }
		
	}

}
