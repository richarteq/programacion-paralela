package semana03;

public class OtroHilo2 extends Thread {
	
	static int FIN = 4;
	
	public OtroHilo2(String str) {
		super(str);
	}
		
	public void run(){
		
		long start = System.nanoTime();
		int descanso = 0;
		int descanso_time = 0;
		
		for (int i = 0; i <= FIN; i++) {
			System.out.println("Posición " + i + ": " + getName());			
			try {
				descanso = (int) (Math.random() * 2000);				
				sleep(descanso);
				descanso_time += descanso; 
				System.out.println("El caballo " + getName() + " descansa.");
			} catch (InterruptedException e) {
				System.out.println(e);				
			}
		}
		long for_time = System.nanoTime() - start;
		
		
		System.out.println("Fin de la carrera para: " + getName() + " " + for_time + " ns");
		System.out.println("Descanso: " + getName() + " " + descanso_time + " ms");
	}	
	
}