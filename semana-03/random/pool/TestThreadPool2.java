package semana03;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class TestThreadPool2 {  
    @SuppressWarnings("resource")
	public static void main(String[] args) {
    	
    	Scanner teclado = new Scanner(System.in);
	    
	    System.out.println("Ingrese cantidad de caballos : ");
	    int n1 = Integer.parseInt(teclado.nextLine()); 
	    
	    ArrayList<String> nombres = new ArrayList<String>(
	    	    Arrays.asList("Tornado", "Pegaso", "Socrates","Espiritu","Galan"));
	    
	    ArrayList<Integer> veces = new ArrayList<Integer>();
	    for(int v=1; v<=nombres.size(); v++) {
	    	veces.add(0);
	    }
	    
	    int randomNum;
	    
       ExecutorService executor = Executors.newFixedThreadPool(n1);//creating a pool of n1 threads  
       for (int i = 1; i <= n1; i++) {  
           //Runnable worker = new WorkerThread("" + i); 
    	   randomNum = 0 + (int)(Math.random() * (nombres.size()-1));
    	   OtroHilo2 worker =  new OtroHilo2(nombres.get(randomNum) + "_" + veces.get(randomNum));
           executor.execute(worker);//calling execute method of ExecutorService
         }
       
       executor.shutdown();  
       while (!executor.isTerminated()) {   }  
 
       System.out.println("Finished all threads");  
   }  
}  