package semana03;

import java.util.Scanner;

public class Hilos2 {

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		
		Scanner teclado = new Scanner(System.in);
	    
	    System.out.println("Ingrese cantidad de caballos : ");
	    int n1 = Integer.parseInt(teclado.nextLine()); 
		
	    for(int i=1; i<=n1; i++) {
	    	new OtroHilo2().start();
	    }
		
	}

}
