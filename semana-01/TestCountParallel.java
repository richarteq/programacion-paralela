package semana01;

public class TestCountParallel{
	
	public static void main(String[] args){
		
		CountParallel obj01 = new CountParallel();
		CountParallel obj02 = new CountParallel();
		CountParallel obj03 = new CountParallel();
		CountParallel obj04 = new CountParallel();
		
		obj01.start();
		obj02.start();
		obj03.start();
		obj04.start();
		
	}
}

//Flujo de control
//1-3-5-7-8-9-10-12-13-14-15-17-18

//12
//?
//13
//?
//14
//?
//15
//?
