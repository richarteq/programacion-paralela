//package semana05;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.ArrayList;
//import java.io.ObjectInputStream;
import java.io.*;

public class EjemploReceptor2 {

	public static void main(String[] args){
		
		// Una aplicación que recibe un mensaje utilizando un socket datagrama
		// sin conexión.
		// Se espera un argumento de línea de mandato:
		// <número de puerto del socket del recptor>
		// Note: se debería especificar el mismo número de puerto
		// en los argumentos de línea de mandato del emisor.
		
		if (args. length != 1)
		System.out.println("Este programa requiere 1 argumentos de linea de mandato");
		else {
			int puerto = Integer.parseInt(args[ 0 ]);
			final int MAX_LON = 65536 ;
			// Esta es la longitud máxima asumida en octetos
			// del datagrama que se va recibir.
			try {
				DatagramSocket miSocket = new DatagramSocket(puerto);
				// Instancia un socket datagrama para recibir los datos
				byte[] almacen = new byte[MAX_LON];
				
				//byte[] sendData = new byte[65536];
				

				DatagramPacket datagrama = new DatagramPacket (almacen, MAX_LON );
				miSocket.receive(datagrama);
				//sendData = serialize(almacen);
				//String mensaje = new String(almacen);
				//ObjectInputStream inputStream = new ObjectInputStream(new ByteArrayInputStream(almacen));
				
				ObjectInputStream inputStream = new ObjectInputStream(new ByteArrayInputStream(almacen));
    			ArrayList<String> list = (ArrayList) inputStream.readObject();

				//ArrayList<String> mensajes = (ArrayList) inputStream.readObject();
				System. out.println(list.size());
				
				miSocket.close( );
			}
			catch (Exception ex) {
				ex.printStackTrace( ) ;
			}

		}
	
	}
	
}
