package semana08;

import java.util.ArrayList;
import java.util.Random;

public class ExampleThread01 {

	public static void main(String[] args) throws InterruptedException {
		
		//int N = 100;
		//Arreglo de 100 numeros enteros
		int bigArray[] = new int[100];
        
		//Recorre todo el arreglo e ingresa numeros aleatorios enteros entre 0-99
		for (int i = 0; i < bigArray.length; i++) {
            bigArray[i] = new Random().nextInt(100); //Aleatorio entre 0-99
        }
		
		//Inicializacion de contenedor de hilos
		ArrayList<Thread> hilos = new ArrayList<Thread>();
		
		//Crea 10 hilos para dividir el trabajo de impresion
        for (int i = 0; i < 10; i++) {
        	hilos.add(new PrintArrayThread(bigArray, i, i + 10));
        	hilos.get(hilos.size()-1).start();
        	//hilos.get(hilos.size()-1).join();
        	
        }
        
        
        hilos.clear();
        
	}

}
