//package semana05;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.ArrayList;
import java.io.Serializable;
import java.io.*;

public class EjemploEmisor3 {
	
	// Una aplicación que manda un mensaje utilizando un socket datagrama
	// sin conexión.
	// Se esperan tres argumentos de línea de mandato, en orden:
	// <nombre de dominio o dirección IP del recepetor>
	// <número del puerto del socket del receptor>
	// <mensaje, una cadena, para mandar>
	
	public static void main(String[ ] args) {
		
			try {
				InetAddress maquinaReceptora = InetAddress. getByName ("192.168.1.103") ;
				int puertoReceptor = Integer.parseInt("8005");
				
				ArrayList<String> mensajes = new ArrayList<String>();
				mensajes.add("Buenos días");
				mensajes.add("Buenos tardes");
				mensajes.add("Buenos noches");
				mensajes.add("Bienvenidos");
				
				// Sender
				//List list = new ArrayList();
				ByteArrayOutputStream out = new ByteArrayOutputStream();
				ObjectOutputStream outputStream = new ObjectOutputStream(out);
				outputStream.writeObject(mensajes);
				outputStream.close();
			
				byte[] listData = out.toByteArray();
				
				DatagramSocket miSocket = new DatagramSocket ( );
				//byte[ ] almacen = mensaje.getBytes( );
				DatagramPacket datagrama = new DatagramPacket(listData, listData.length,maquinaReceptora, puertoReceptor);
				miSocket.send(datagrama);
				miSocket.close( );
			}
			catch (Exception ex) {
				ex.printStackTrace( ) ;
			}

		
	}

}
