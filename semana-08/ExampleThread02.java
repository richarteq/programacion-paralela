package semana08;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExampleThread02 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int bigArray[] = new int[100];
		
		
        for (int i = 0; i < bigArray.length; i++) {
            bigArray[i] = new Random().nextInt(100);
        }
        
        ExecutorService executor = Executors.newFixedThreadPool(3);
        for (int i = 0; i < 10; i++) {
        	Thread worker = new PrintArrayThread(bigArray, i, i + 10);
        	executor.execute(worker);
        }
        
        executor.shutdown();
        while (!executor.isTerminated()) {
        }
        System.out.println("Finished all threads");
	}

}
