package semana08;

public class PrintArrayThread extends Thread {
	
	//Inicializa arreglo de enteros
    private int[] array;
    //A partir de que indice empiezo
    private int start;
    //Hasta que indice se trabajara
    private int end;
 
    public PrintArrayThread(int[] array, int start, int end) {
        this.array = array;
        this.start = start;
        this.end = end;
    }
 
    public void run() {
    	//Recorre el arreglo desde un inicio hasta un final (porcion del arreglo)
        for (int i = start; i < end; i++) {
            System.out.println(
                    String.format("Current thread %s printing value %s", Thread.currentThread().getName(), array[i]));
        }
    }
 
}
